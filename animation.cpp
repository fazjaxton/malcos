#include "animation.h"

RotateMotion::RotateMotion(float rotations)
{
    mRotation = -rotations * 360;
}
  
void RotateMotion::update(sf::Time now, sf::Transformable & object) {
    auto percent = getPercent(now);
    object.setRotation(mRotation * (1.0f - percent));
    setComplete(percent >= 1.0);
}


LinearMotion::LinearMotion(sf::Vector2i const & startPos, sf::Vector2i const & endPos)
  : mStartPos(startPos)
  , mEndPos(endPos)
{ }

void LinearMotion::update(sf::Time now, sf::Transformable & object) {
    auto percent = getPercent(now);
    int x = (mEndPos.x - mStartPos.x) * percent + mStartPos.x;
    int y = (mEndPos.y - mStartPos.y) * percent + mStartPos.y;

    object.setPosition(x, y);
    setComplete(percent >= 1.0);
}
