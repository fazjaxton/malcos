#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>

#include <list>

#include "random.h"

struct Animation {
    Animation() { }

    virtual ~Animation() {}

    virtual void update(sf::Time now, sf::Transformable & object) = 0;
    bool isComplete() { return mComplete; }

    void setStartTime(sf::Time time) { mStartTime = time; }
    void setDuration(sf::Time time) { mDuration = time; }

protected:
    float getPercent(sf::Time now) {
        float percent = (now - mStartTime) / mDuration;
        return (percent >= 1.0f) ? 1.0f : percent;
    }

    void setComplete(bool complete) { mComplete = complete; }

private:
    sf::Time mStartTime;
    sf::Time mDuration;
    bool mComplete = false;
};


struct RotateMotion : public Animation {
    RotateMotion(float rotations);
    virtual ~RotateMotion() { }

    virtual void update(sf::Time now, sf::Transformable & object) final;

private:
    float mRotation;
};


struct LinearMotion : public Animation {
    LinearMotion(sf::Vector2i const & startPos, sf::Vector2i const & endPos);
    virtual ~LinearMotion() { }

    virtual void update(sf::Time now, sf::Transformable & object) final;

private:
    sf::Vector2i mStartPos;
    sf::Vector2i mEndPos;
};

#endif
