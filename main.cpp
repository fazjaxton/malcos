#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <algorithm>
#include <cctype>
#include <iostream>
#include <list>
#include <unordered_set>

#include "animation.h"
#include "errorf.h"
#include "picture.h"
#include "texture.h"

struct Entity {
    virtual ~Entity() { }

    virtual bool update(sf::Time now) { return true; };
    virtual void draw(sf::RenderTarget & target) { };
};

struct PictureAnimator : public Entity {
    using AnimationPtr = std::unique_ptr<Animation>;

    PictureAnimator(Picture & picture)
      : mPicture(picture)
    { } 

    virtual ~PictureAnimator() { }

    virtual bool update(sf::Time /*now*/) override {
        bool complete = true;
        sf::Time now = mClock.getElapsedTime();

        for (auto it = mAnimations.begin(); it != mAnimations.end(); ++it) {
            auto & animation = **it;
            if (!animation.isComplete()) {
                animation.update(now, mPicture.sprite());
                complete = false;
            }
        }

        return complete;
    }

    virtual void draw(sf::RenderTarget & target) override {
        mPicture.draw(target);
    }

    Animation & addAnimation(AnimationPtr animation) {
        animation->setStartTime(mClock.getElapsedTime());
        return *mAnimations.emplace_back(std::move(animation));
    }

    Picture & picture() {
        return mPicture;
    }

private:
    Picture & mPicture;
    std::vector<AnimationPtr> mAnimations;
    sf::Clock mClock;
};

struct MalcOS {
    void loadTexturesFrom(std::string directory) {
        TextureScanner scanner(directory);
        scanner.scan();
        mTextureRepo.addFromScanner(scanner);
    }

    sf::Vector2i getRandomStart(unsigned offset) {
        sf::Vector2i start;
        if (mRandom.generate(2)) {
            if (mRandom.generate(2)) {
                start.y = mSize.y + 50;
            }
            else {
                start.y = -50;
            }
            start.x = mRandom.generate(mSize.x);
        }
        else {
            if (mRandom.generate(2)) {
                start.x = mSize.x + 50;
            }
            else {
                start.x = -50;
            }
            start.y = mRandom.generate(mSize.y);
        }

        return start;
    }

    sf::Vector2i getRandomEnd() {
        return sf::Vector2i(mRandom.generate(mSize.x),
                         mRandom.generate(mSize.y));
    }

    void addAnimations(PictureAnimator & animator) {
        auto start = getRandomStart(50);
        auto end = getRandomEnd();

        auto & animation = animator.addAnimation(
                    std::make_unique<LinearMotion>(
                        start,
                        end
                    )
                );
        animation.setDuration(sf::seconds(2));

        auto & animation2 = animator.addAnimation(
                    std::make_unique<RotateMotion>(
                        (float)mRandom.generate(300) / 200
                    )
                );
        animation2.setDuration(sf::seconds(2));
    }

    void addRandomPicture() {
        if (mTextureRepo.size() == 0) {
            throw std::runtime_error("No textures loaded");
        }

        auto texIdx = mRandom.generate(mTextureRepo.size());
        auto & picturePtr = mPictures.emplace_back(
                                std::make_unique<Picture>(mTextureRepo.getTexture(texIdx))
                            );

        auto & animator = mAnimators.emplace_back(*picturePtr.get());
        addAnimations(animator);
    }

    void addPictureToBase(Picture & picture) {
        auto const & image = mTextureRepo.getImageFromTexture(picture.texture());
        sf::Vector2f position = picture.sprite().getPosition();
        position -= picture.sprite().getOrigin();
        sf::IntRect rect = picture.sprite().getTextureRect();

        if (position.x < 0) {
            int correction = -position.x;
            if (correction >= rect.width) {
                return;
            }
            rect.left  += correction;
            rect.width -= correction;
            position.x = 0;
        }
        if (position.y < 0) {
            int correction = -position.y;
            if (correction >= rect.height) {
                return;
            }
            rect.top    += correction;
            rect.height -= correction;
            position.y = 0;
        }
        if (position.y + rect.height > mBaseImage.getSize().y) {
            int correction = position.y + rect.height - mBaseImage.getSize().y;
            if (correction > rect.height) {
                // Image is off screen
                return;
            }
            rect.height -= correction;
        }
        if (position.x + rect.width > mBaseImage.getSize().x) {
            int correction = position.x + rect.width - mBaseImage.getSize().x;
            if (correction > rect.width) {
                // Image is off screen
                return;
            }
            rect.width -= correction;
        }

        mBaseImage.copy(*image, position.x, position.y, rect);
        mBaseTexture.update(mBaseImage);
    }

    void update() {
        mCompleteAnimators.clear();

        sf::Time now = mClock.getElapsedTime();
        for (auto it = mAnimators.begin(); it != mAnimators.end(); ++it) {
            bool complete = it->update(now);
            if (complete) {
                mCompleteAnimators.push_back(it);
            }
        }
    }

    void draw(sf::RenderWindow & window) {
        window.draw(mBaseSprite);
        for (auto & a : mAnimators) {
            a.draw(window);
        }

        for (auto & it : mCompleteAnimators) {
            addPictureToBase(it->picture());
            mAnimators.erase(it);
        }
    }

    void setSize(sf::Vector2u size) {
        mSize = size;
        mBaseImage.create(size.x, size.y, sf::Color(0, 0, 0));
        mBaseTexture.loadFromImage(mBaseImage);
        mBaseSprite.setTexture(mBaseTexture, true);
    }

private:
    using PicturePtr = std::unique_ptr<Picture>;
    using AnimatorIterator = std::list<PictureAnimator>::iterator;

    TextureRepository mTextureRepo;
    RandomGenerator<size_t> mRandom;

    std::list<PicturePtr> mPictures;
    std::list<PictureAnimator> mAnimators;
    std::list<AnimatorIterator> mCompleteAnimators;

    sf::Image mBaseImage;
    sf::Texture mBaseTexture;
    sf::Sprite mBaseSprite;
    sf::Vector2u mSize;
    sf::Clock mClock;
};

int main() {
    sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
    sf::RenderWindow window(desktop, "MalcOS", sf::Style::Fullscreen);
    window.setVerticalSyncEnabled(true);

    MalcOS malcos;
    malcos.loadTexturesFrom("images/");
    malcos.setSize(window.getSize());

    sf::Clock clock;

    while (window.isOpen()) {
        sf::Event event;
        
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::KeyPressed:
                    malcos.addRandomPicture();
                    break;
                default:
                    break;
            }
        }

        window.clear();
        malcos.update();
        malcos.draw(window);
        window.display();

        auto elapsed = clock.restart();
        if (elapsed < sf::microseconds(16666)) {
            sf::sleep(sf::microseconds(16666) - elapsed);
        }
    }

    return 0;
}
