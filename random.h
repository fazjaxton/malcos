#ifndef RANDOM_H
#define RANDOM_H

#include <random>

template <typename IntType = unsigned int>
struct RandomGenerator {
    RandomGenerator(IntType max = std::numeric_limits<IntType>::max())
      : device()
      , engine(device())
      , uniform(0, max)
    { }

    IntType generate(IntType min, IntType max) {
        auto const range = max - min;
        return (uniform(engine) % range) + min;
    }

    IntType generate(IntType max) {
        return uniform(engine) % max;
    }

    IntType generate() {
        return uniform(engine);
    }

    RandomGenerator(RandomGenerator const &) = delete;
    RandomGenerator & operator=(RandomGenerator const &) = delete;

private:
    std::random_device device;
    std::default_random_engine engine;
    std::uniform_int_distribution<IntType> uniform;
};

#endif
