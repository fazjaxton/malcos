TARGET:=malcos
OBJECTS:=main.o animation.o
DEPS:=$(OBJECTS:%.o=%.d)

MAGICKFLAGS:=$(shell pkg-config --cflags Magick++)
SFMLFLAGS:=$(shell pkg-config --cflags sfml-all)
MAGICKLIBS:=$(shell pkg-config --libs Magick++)
SFMLLIBS:=$(shell pkg-config --libs sfml-all)

CPPFLAGS:=-o0 -std=c++17 -Wall -g $(MAGICKFLAGS) $(SFMLFLAGS)
LDFLAGS:=$(MAGICKLIBS) $(SFMLLIBS)

$(TARGET): $(OBJECTS)
	clang++ $(LDFLAGS) -o $@ $^

%.o: %.cpp
	clang++ $(CPPFLAGS) -MD -c -o $@ $<

all: $(TARGET)

-include $(DEPS)

clean:
	rm -f $(TARGET) *.o *.d

.PHONY: all clean
.DEFAULT: all
