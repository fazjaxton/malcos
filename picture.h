#ifndef PICTURE_H
#define PICTURE_H

#include <SFML/Window.hpp>

#include <memory>

struct Picture {
    Picture(std::shared_ptr<sf::Texture> texture)
      : mTexture(texture)
      , mSprite(*mTexture)
    {
        mSprite.setOrigin(mTexture->getSize().x / 2, mTexture->getSize().y / 2);
    }

    Picture & operator=(Picture const & other) {
        mTexture = other.mTexture;
        mSprite.setTexture(*mTexture);

        // Neither sprite nor transform provides a useful copy constructor, so we have to
        // manually copy all these attributes
        mSprite.setTextureRect(other.mSprite.getTextureRect());
        mSprite.setColor(other.mSprite.getColor());
        mSprite.setOrigin(other.mSprite.getOrigin());
        mSprite.setPosition(other.mSprite.getPosition());
        mSprite.setRotation(other.mSprite.getRotation());
        mSprite.setScale(other.mSprite.getScale());

        return *this;
    }

    Picture(Picture const & other)          { *this = other; }
    Picture(Picture && other)               { *this = other; }
    Picture & operator=(Picture && other)   { *this = other; return *this; }

    void draw(sf::RenderTarget & window) {
        window.draw(mSprite);
    }

    sf::Sprite & sprite() { return mSprite; }
    std::shared_ptr<sf::Texture> const & texture() { return mTexture; }

private:
    std::shared_ptr<sf::Texture> mTexture;
    sf::Sprite mSprite;
};

#endif
