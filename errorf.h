#ifndef ERRORF_H
#define ERRORF_H

#include <stdarg.h>
#include <cstdio>

std::runtime_error errorf(const char * fmt, ...) {
    char buffer[1024];
    va_list args;

    va_start(args, fmt);
    int len = vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);

    if (len > sizeof(buffer) - 1)
        len = sizeof(buffer) - 1;

    buffer[len] = 0;

    return std::runtime_error(buffer);
};

#endif
