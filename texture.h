#ifndef TEXTURE_H
#define TEXTURE_H

#include <SFML/Graphics.hpp>

#include <filesystem>

struct TextureScanner {
    using ImageFilenames = std::unordered_set<std::string>;

    TextureScanner(std::string const & path)
      : mDirPath(path)
    {
        if (!std::filesystem::exists(mDirPath)) {
            throw std::runtime_error("Directory not found");
        }
        if (!std::filesystem::is_directory(mDirPath)) {
            throw std::runtime_error("Is not directory");
        }
    }

    ImageFilenames const & getImageFilenames() const { 
        return mImageFilenames;
    }

    void toLower(std::string & str) const {
        std::transform(str.begin(), str.end(), str.begin(),
                [](unsigned char c) { return std::tolower(c); });
    }

    void scan() {
        for (auto entry : std::filesystem::directory_iterator(mDirPath)) {
            std::cout << entry << std::endl;
            if (!entry.exists() || !entry.is_regular_file()) {
                continue;
            }
            std::string file = entry.path().string();

            auto dotIdx = file.find_last_of('.');
            if (dotIdx == std::string::npos) {
                continue;
            }

            std::string extension = file.substr(dotIdx + 1);
            toLower(extension);

            if (extension == "jpg" || extension == "png") {
                mImageFilenames.insert(file);
            }
        }
    }

private:
    std::filesystem::path mDirPath;
    ImageFilenames mImageFilenames;
};

struct TextureRepository {
    using TexturePtr = std::shared_ptr<sf::Texture>;
    using ImagePtr = std::shared_ptr<sf::Image>;

    void add(std::string const & filename) {
        ImagePtr image = std::make_shared<sf::Image>();
        if (!image->loadFromFile(filename)) {
            return;
        }

        TexturePtr texture = std::make_shared<sf::Texture>();
        if (!texture->loadFromImage(*image)) {
            return;
        }

        mImages.emplace_back(std::move(image));
        mTextures.emplace_back(std::move(texture));
    }

    void addFromScanner(TextureScanner const & scanner) {
        for (auto const & filename : scanner.getImageFilenames()) {
            add(filename);
        }
    }

    size_t size() const { return mTextures.size(); }

    TexturePtr getTexture(size_t idx) const { return mTextures.at(idx); }
    ImagePtr   getImage(size_t idx)   const { return mImages.at(idx); }

    ImagePtr const & getImageFromTexture(TexturePtr const & texture) const {
        auto textureIt = std::find(mTextures.begin(), mTextures.end(), texture);
        if (textureIt == mTextures.end()) {
            throw std::runtime_error("Cannot find texture in repo");
        }
        int distance = std::distance(mTextures.begin(), textureIt);
        return mImages.at(distance);
    }

private:
    std::vector<TexturePtr> mTextures;
    std::vector<ImagePtr> mImages;
};


#endif
